import Vue from 'vue'
import App from './App.vue'
import router from './router'

// 导入ElementUI库
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

// 导入message
import { Message } from 'element-ui';
Vue.prototype.$message = Message

// 导入echarts
import 'echarts'
import Echarts from 'vue-echarts'
Vue.component('v-chart',Echarts);

// 导入全局样式
import './assets/css/global.css'

// 导入axios
import axios from 'axios'
// 配置请求的根路径
axios.defaults.baseURL = 'http://127.0.0.1:8888/api'
Vue.prototype.$http = axios

Vue.config.productionTip = false

// 配置请求头(aixos请求拦截器)
axios.interceptors.request.use(config => {
    // console.log(config)
    // 为请求头挂载Authorization字段
    config.headers.Authorization = window.sessionStorage.getItem('token')
        // 必须返回请求数据
    return config
})

Vue.filter('dateFormat', function(originVal) {
    const dt = new Date(originVal)

    const y = dt.getFullYear()
    const m = (dt.getMonth() + 1 + '').padStart(2, '0')
    const d = (dt.getDate() + '').padStart(2, '0')

    const hh = (dt.getHours() + '').padStart(2, '0')
    const mm = (dt.getMinutes() + '').padStart(2, '0')
    const ss = (dt.getSeconds() + '').padStart(2, '0')

    return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
});



new Vue({
    router,
    render: h => h(App)
}).$mount('#app')