const db = require('../db/mysql')

// 数据库请求问题都在这解决
module.exports = function(sql, params) {
    // 创建new promise对象解决回调地狱的问题
    return new Promise((resolve, reject) => {
        db.query(sql, params, (err, results) => {
            if (err) {
                reject(err)
            } else {
                resolve(results)
            }
        })
    })
}