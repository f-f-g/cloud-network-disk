const joi = require('joi')

const username = joi.string().alphanum().min(2).max(8).required();
const password = joi.string().pattern(/^[\S]{6,16}$/).required();
// 定义 id, nickname 的验证规则
const id = joi.number().integer().min(1).required();
const nickname = joi.string().required();
// dataUri() 指的是如下格式的字符串数据：
// data:image/png;base64,VE9PTUFOWVNFQ1JFVFM=
const avatar = joi.string().dataUri().required();

// 验证登录数据的验证方法
exports.login_schema = {
    body: {
        username,
        password
    }
}

// 验证注册数据的验证方法
exports.reguser_schema = {
    body: {
        username,
        password,
        nickname
    }
}

// 更新用户的验证规则对象
exports.update_userinfo_schema = {
    body: {
        // 定义 id, nickname, emial 的验证规则
        id,
        nickname
    }
}

// 重置密码的验证规则对象
exports.update_password_schema = {
    body: {
        // 使用 password 这个规则，验证 req.body.oldPwd 的值
        oldPwd: password,
        // 使用 joi.not(joi.ref('oldPwd')).concat(password) 规则，验证 req.body.newPwd 的值
        // 解读：
        // 1. joi.ref('oldPwd') 表示 newPwd 的值必须和 oldPwd 的值保持一致
        // 2. joi.not(joi.ref('oldPwd')) 表示 newPwd 的值不能等于 oldPwd 的值
        // 3. .concat() 用于合并 joi.not(joi.ref('oldPwd')) 和 password 这两条验证规则
        newPwd: joi.not(joi.ref('oldPwd')).concat(password),
    }
}

// 更新头像的验证规则对象
exports.update_avatar_schema = {
    body: {
        avatar
    }
}

exports.createManager_schema ={
  body:{
  username: joi.string().alphanum().min(2).max(8).required(),
  password: joi.string().pattern(/^[\S]{6,16}$/).required(),
  mobile: joi.string().pattern(/^1[3-9]\d{9}$/).required(),
  email: joi.string().email({
      minDomainSegments: 2,
      tlds: { allow: ["com", "net"] }
  }).required()
  }
}