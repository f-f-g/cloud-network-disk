const express = require('express')
const app = express()
const port = 8888

// 过滤数据
const bodyparser = require('body-parser')
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({ extended: true }))

//开放upload静态文件
var path = require('path');
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'upload')));

// 跨域
const cors = require('cors')
app.use(cors())

// 状态回报函数
app.use((req, res, next) => {
    // status状态200为成功，500为失败，默认为500
    res.ResInfo = function(err, status = 500, resData = {}) {
        res.send({
            data: resData,
            meta: {
                status,
                msg: err instanceof Error ? err.message : err,
            }
        })
    }
    next()
})

// 路由区域开始

// 用户的路由
const user = require('./router/user/user');
app.use('/api', user)

const Manager = require('./router/manager/manager')
app.use('/api', Manager)

const file = require('./router/file/file')
app.use('/api', file)

// 数据验证包
const Joi = require('joi')

// 错误汇报机制
app.use((err, req, res, next) => {
    if (err instanceof Joi.ValidationError) return res.ResInfo(err)
    if (err.name == 'UnauthorizedError') return res.ResInfo("用户认证失败")
    next()
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))