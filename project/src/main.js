import Vue from 'vue'
import App from './App.vue'
import router from './router'

// 导入element-ui
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

// 导入全局样式表
import "./assets/css/global.css"

// 导入axios
import axios from 'axios'
// 配置请求的根路径
axios.defaults.baseURL = 'http://127.0.0.1:8888/api'
Vue.prototype.$http = axios

// 导入message
import { Message } from 'element-ui';
Vue.prototype.$message = Message

Vue.config.productionTip = false

// 配置请求头(aixos请求拦截器)
axios.interceptors.request.use(config => {
  // console.log(config)
  // 为请求头挂载Authorization字段
  config.headers.Authorization = window.sessionStorage.getItem('token')
  // 必须返回请求数据
  return config
})

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')